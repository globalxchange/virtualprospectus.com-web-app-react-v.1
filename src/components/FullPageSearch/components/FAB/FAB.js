import React, { useState } from 'react'
import styles from './_fab.scss'

function FAB() {
  const [hover, setHover] = useState(false)
  return (
    <div
      className={`${styles.fabWrapper} ${hover && styles.true}`}
      onMouseEnter={() => {
        setHover(true)
        setTimeout(() => {
          setHover(true)
        }, 4002)
      }}
      onMouseLeave={() => {
        setTimeout(() => {
          setHover(false)
        }, 4000)
      }}
    >
      <img
        src='https://drivetest.globalxchange.io/gxsharepublic/?full_link=bets.brain.stream/3d56bc81fcc3bce7be4ee35160e30a85'
        alt=''
      />
      <div className={styles.text}>Buy Crypto</div>
    </div>
  )
}

export default FAB
