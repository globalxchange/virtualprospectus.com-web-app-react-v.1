import React from 'react'
import Scrollbars from 'react-custom-scrollbars'
import styles from './categoryModal.module.scss'
function CategoryModal({ categories, onClose, label }) {
  return (
    <div className={`categoryModalWrap ${styles.categoryModalWrap}`}>
      <div
        className={styles.overlay}
        onClick={() => {
          try {
            onClose()
          } catch (error) {}
        }}
      />
      <div className={styles.categoryModal}>
        <div className={`head ${styles.head}`}>{label || 'Category'}</div>
        {categories?.length > 4 ? (
          <Scrollbars className={styles.scrollBar}>
            {categories?.map((cat, i) => (
              <div
                key={`${cat.name}${i}`}
                className={`${styles.item} ${cat.selected && styles.true}`}
                onClick={() => {
                  try {
                    cat.onClick()
                    onClose()
                  } catch (error) {}
                }}
              >
                <div className={styles.imgWrap}>
                  <svg viewBox='0 0 1 1' />
                  <img className={styles.image} src={cat?.img} alt='' />
                </div>
                <div className={styles.label}>{cat?.label}</div>
              </div>
            ))}
            <div className={styles.itemHid} />
            <div className={styles.itemHid} />
          </Scrollbars>
        ) : (
          <div className={styles.categoriesFour}>
            {categories?.map((cat, i) => (
              <div
                key={`${cat.name}${i}`}
                className={`${styles.item} ${cat.selected && styles.true}`}
                onClick={() => {
                  try {
                    cat.onClick()
                    onClose()
                  } catch (error) {}
                }}
              >
                <div className={styles.imgWrap}>
                  <svg viewBox='0 0 1 1' />
                  <img className={styles.image} src={cat?.img} alt='' />
                </div>
                <div className={styles.label}>{cat?.label}</div>
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  )
}

export default CategoryModal
