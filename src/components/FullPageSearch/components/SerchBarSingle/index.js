import React, { useEffect, useRef } from 'react'
import { ReactComponent as SearchIcon } from '../../static/images/search.svg'
import styles from './serchbarsingle.module.scss'

function SerchBarSingle({ search, setSearch, placeholder, iconOne, onClick }) {
  const ref = useRef()
  useEffect(() => {
    ref?.current && ref.current.focus()
  }, [])
  return (
    <div className={`${styles.search} search`}>
      <div
        className={`${styles.icon} ${iconOne?.active && styles.true}`}
        onClick={() => {
          try {
            iconOne.onClick()
          } catch (error) {}
        }}
      >
        <img src={iconOne?.icon} alt='' />
        <span>{iconOne?.name}</span>
      </div>
      <input
        ref={ref}
        type='text'
        className={styles.textInput}
        placeholder={placeholder}
        value={search}
        onChange={(e) => setSearch(e.target.value)}
      />
      <SearchIcon
        className={styles.searchIcn}
        onClick={() => {
          try {
            onClick()
          } catch (error) {}
        }}
      />
    </div>
  )
}

export default SerchBarSingle
