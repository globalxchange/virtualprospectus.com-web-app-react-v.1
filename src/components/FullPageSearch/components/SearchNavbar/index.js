import React, { useEffect, useRef } from 'react'
import { ReactComponent as SearchIcon } from '../../static/images/search.svg'
import { ReactComponent as PasteIcon } from '../../static/images/paste.svg'
import { ReactComponent as BackIcon } from '../../static/images/back.svg'

import styles from './searchNavbar.module.scss'

function SearchNavbar({
  appLogo,
  iconOne,
  iconTwo,
  search,
  setSearch,
  onClick,
  placeholder
}) {
  const ref = useRef()
  useEffect(() => {
    ref.current && ref.current.focus()
  }, [])
  return (
    <nav className={styles.searchNavbar}>
      <img
        src={appLogo}
        alt='cryptobanklogo'
        className={styles.cryptoImg}
        onClick={() => {
          try {
            onClick()
          } catch (error) {}
        }}
      />
      <BackIcon
        className={styles.backLogo}
        onClick={() => {
          try {
            onClick()
          } catch (error) {}
        }}
      />
      <label className={styles.navSearch}>
        <input
          ref={ref}
          type='text'
          className={styles.textInput}
          placeholder={placeholder}
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        <PasteIcon
          className={styles.clipIcon}
          onClick={() => {
            navigator.clipboard.readText().then((text) => setSearch(text))
          }}
        />
        <SearchIcon className={styles.searchIcn} />
        {iconOne.icon && (
          <div
            className={`${styles.icon} ${iconOne.active && styles.true}`}
            onClick={() => {
              try {
                iconOne.onClick()
              } catch (error) {}
            }}
          >
            <img src={iconOne.icon} alt='' />
            <span>{iconOne.label}</span>
          </div>
        )}
        {iconTwo.icon && (
          <div
            className={`${styles.icon} ${iconTwo.active && styles.true}`}
            onClick={() => {
              try {
                iconTwo.onClick()
              } catch (error) {}
            }}
          >
            <img src={iconTwo.icon} alt='' />
            <span>{iconTwo.label}</span>
          </div>
        )}
      </label>
    </nav>
  )
}

export default SearchNavbar
