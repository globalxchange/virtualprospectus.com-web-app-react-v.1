import React, { Fragment, useEffect, useState } from 'react'
import Searchbar from '../Searchbar'
import SearchView from '../SearchView'

function SearchComponent({
  search,
  setSearch,
  loading,
  appLogo,
  searchList,
  iconOne,
  iconTwo,
  placeholder
}) {
  const [openSearch, setOpenSearch] = useState(false)
  useEffect(() => {
    if (search) {
      setOpenSearch(true)
    }
  }, [search])
  return (
    <Fragment>
      {openSearch ? (
        <SearchView
          search={search}
          setSearch={setSearch}
          placeholder={placeholder}
          iconOne={iconOne}
          iconTwo={iconTwo}
          loading={loading}
          appLogo={appLogo}
          searchList={searchList}
          goBack={() => setOpenSearch(false)}
        />
      ) : (
        <Searchbar
          search={search}
          setSearch={setSearch}
          placeholder={placeholder}
          iconOne={iconOne}
          iconTwo={iconTwo}
        />
      )}
    </Fragment>
  )
}

export default SearchComponent
