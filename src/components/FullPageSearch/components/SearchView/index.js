import React from 'react'
import Scrollbars from 'react-custom-scrollbars'
import Skeleton from 'react-loading-skeleton'

import SearchNavbar from '../SearchNavbar'
import styles from './searchView.module.scss'

function SearchView({
  search,
  setSearch,
  loading,
  appLogo,
  searchList,
  goBack,
  iconOne,
  iconTwo,
  placeholder
}) {
  return (
    <div className={styles.searchPage}>
      <SearchNavbar
        search={search}
        setSearch={setSearch}
        appLogo={appLogo}
        onClick={() => {
          goBack()
        }}
        iconOne={iconOne}
        iconTwo={iconTwo}
        placeholder={placeholder}
      />
      <Scrollbars
        className={styles.scrollList}
        renderThumbVertical={() => <div />}
      >
        {loading
          ? Array(6)
              .fill('')
              .map((a, i) => (
                <div className={styles.bankItem} key={i}>
                  <Skeleton className={styles.logo} />
                  <Skeleton className={styles.bankName} width={420} />
                  <Skeleton className={styles.country} />
                </div>
              ))
          : searchList.map((item) => (
              <div
                className={styles.bankItem}
                key={item._id}
                onClick={() => {
                  try {
                    item.onClick()
                  } catch (error) {}
                }}
              >
                <img className={styles.logo} src={item?.logo} alt='' />
                <div className={styles.text}>
                  <span className={styles.bankName}>{item?.text}</span>
                  <div className={styles.country}>{item?.subText}</div>
                </div>
              </div>
            ))}
      </Scrollbars>
    </div>
  )
}

export default SearchView
