import React, { useState } from "react";

import initialExchange from "../../../static/images/offeringIcons/initialExchange.svg";
import initialFund from "../../../static/images/offeringIcons/initialFund.svg";
function LandingPageSidebar({ menuOpen, setMenuOpen }) {
  const [fundSelected, setFundSelected] = useState("");
  function renderList() {
    switch (fundSelected) {
      case "fund":
        return (
          <div className="videoContent">
            <div className="logoHead">
              <img src={initialFund} alt="" className="headLogo" />
            </div>
            <svg
              className="video"
              viewBox="0 0 375 215"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect width="376" height="215" fill="black" fill-opacity="0.57" />
              <path
                d="M211.705 105.257L168.999 84.0566C168.172 83.6467 167.163 83.6561 166.346 84.0754C165.524 84.4994 165.022 85.2721 165.022 86.1107V128.512C165.022 129.35 165.524 130.123 166.346 130.547C166.762 130.759 167.227 130.867 167.691 130.867C168.14 130.867 168.593 130.768 168.999 130.566L211.705 109.365C212.543 108.946 213.066 108.164 213.066 107.311C213.066 106.458 212.543 105.676 211.705 105.257Z"
                fill="white"
              />
            </svg>
            <div className="fundDetail">
              <div className="title">What Is A Fund Offering? </div>
              <p className="desc">
                The Initial Fund Offering (IFO) is a system which allows the
                user to invest into a hedge fund manager trustless while at the
                same time creating liquidity for the invested stake in the
                fund... <span>Read More</span>
              </p>
              <div className="btnLearnMore">Learn More</div>
              <div className="btnCreate">Create An IFO</div>
            </div>
          </div>
        );
      case "exchange":
        return (
          <div className="videoContent">
            <div className="logoHead">
              <img src={initialExchange} alt="" className="headLogo" />
            </div>
            <svg
              className="video"
              viewBox="0 0 375 215"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect width="376" height="215" fill="black" fill-opacity="0.57" />
              <path
                d="M211.705 105.257L168.999 84.0566C168.172 83.6467 167.163 83.6561 166.346 84.0754C165.524 84.4994 165.022 85.2721 165.022 86.1107V128.512C165.022 129.35 165.524 130.123 166.346 130.547C166.762 130.759 167.227 130.867 167.691 130.867C168.14 130.867 168.593 130.768 168.999 130.566L211.705 109.365C212.543 108.946 213.066 108.164 213.066 107.311C213.066 106.458 212.543 105.676 211.705 105.257Z"
                fill="white"
              />
            </svg>
            <div className="fundDetail">
              <div className="title">What Is A Exchange Offering? </div>
              <p className="desc">
                The Initial Exchange Offering (IEO) is a system which allows the
                user to you to structure any investment oppurtunity as a
                tokenized asset that anyone can buy and trade on your Agency..{" "}
                <span>Read More</span>
              </p>
              <div className="btnLearnMore">Learn More</div>
              <div className="btnCreate">Create An IEO</div>
            </div>
          </div>
        );

      default:
        return (
          <>
            <div className="brandCard" onClick={() => setFundSelected("fund")}>
              <img src={initialFund} alt="" />
            </div>
            <div className="brandCard mb-auto">
              <img src={initialExchange} alt="" />
            </div>
            <div className="btAdd">Select One</div>
          </>
        );
    }
  }
  return (
    <div className={`sideBar ${menuOpen}`}>
      {menuOpen && (
        <div
          className="closeCross"
          onClick={() => {
            setMenuOpen(false);
            setFundSelected("");
          }}
        />
      )}
      {renderList()}
    </div>
  );
}

export default LandingPageSidebar;
