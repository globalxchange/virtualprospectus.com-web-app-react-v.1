import React, { useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import virtualProspectus from "../../static/images/logos/virtualProspectus.svg";
import styles from "./modalCoinSelect.module.scss";

function ChartTypeSelectModal({ type, setType, onClose }) {
  const [searchStr, setSearchStr] = useState("");
  return (
    <>
      <div className={styles.modalCountrySelect}>
        <div className={styles.overlayClose} onClick={() => onClose()} />
        <div className={styles.modalContent}>
          <div className={styles.head}>
            <img src={virtualProspectus} alt="" />
          </div>
          <div className={styles.content}>
            <input
              value={searchStr}
              type="text"
              placeholder={`Search Data Types.....|`}
              className={styles.searchCountry}
              onChange={(e) => setSearchStr(e.target.value)}
            />
            <Scrollbars
              className={styles.countryListScroll}
              renderThumbHorizontal={() => <div />}
              renderThumbVertical={() => <div />}
              renderView={(props) => (
                <div {...props} className={styles.countryList} />
              )}
            >
              <div
                className={`${styles.listCountry} ${styles.true}`}
                onClick={() => {
                  setType("Market Cap");
                  onClose();
                }}
              >
                <div className={styles.name}>Market Cap</div>
              </div>
              <div
                className={`${styles.listCountry} ${styles.dis}`}
                onClick={() => {
                  setType("Market Cap");
                  onClose();
                }}
              >
                <div className={styles.name}>Token Price</div>
              </div>
              <div
                className={`${styles.listCountry} ${styles.dis}`}
                onClick={() => {
                  setType("Market Cap");
                  onClose();
                }}
              >
                <div className={styles.name}>Trade Volume</div>
              </div>
              <div
                className={`${styles.listCountry} ${styles.dis}`}
                onClick={() => {
                  setType("Market Cap");
                  onClose();
                }}
              >
                <div className={styles.name}>Profit & Loss</div>
              </div>
            </Scrollbars>
          </div>
          <div className={styles.footer}>
            <div
              className={`${styles.btnType} `}
              onClick={() => {
                onClose();
              }}
            >
              Close
            </div>
            <div
              className={`${styles.btnType} ${styles.true}`}
              onClick={() => {
                onClose();
              }}
            >
              Save
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ChartTypeSelectModal;
