export const GX_API_ENDPOINT = 'https://comms.globalxchange.io'

export const APP_CODE = 'ice'

export const NEW_CHAT_API = 'https://testchatsioapi.globalxchange.io'

export const NEW_CHAT_SOCKET = 'https://testsockchatsio.globalxchange.io'

export const SUPPORT_CHAT_URL = 'https://chatsapi.globalxchange.io/gxchat/'
